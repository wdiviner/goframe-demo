package main

import (
	// 导入 service 的 interface
	_ "demo/internal/logic"
	_ "demo/internal/packed"
	// 导入 mysql 的驱动
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	"github.com/gogf/gf/v2/os/gctx"

	"demo/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.New())
}
