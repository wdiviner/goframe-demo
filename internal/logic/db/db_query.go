package db

import (
	"demo/internal/model"
	"fmt"
	"github.com/gogf/gf/v2/frame/g"
)

// QuerySingleRecord 查询单条记录
func (s *sDB) QuerySingleRecord(uid int64, mobile string) (res interface{}, err error) {
	// 多个 where 是 and 查询
	result, err := g.Model("tb_user").
		Fields("uid, username, mobile, insert_at").
		Where("uid = ?", uid).Where("mobile = ?", mobile).
		One()
	// One  用于查询并返回单条记录

	return result, err
}

// QueryMultiRecord 查询多条记录
func (s *sDB) QueryMultiRecord() (res interface{}, err error) {
	resAll, err := g.Model("tb_user").
		Fields("uid, username, mobile").
		Where("is_delete = ?", 0).
		Limit(0, 10).
		OrderAsc("uid").
		All()

	// All  用于查询并返回多条记录的列表/数组
	fmt.Printf("Use All() function : %+v", resAll)

	// Value 要结合 fields 来使用
	resVal, err := g.Model("tb_user").
		Fields("uid, username, mobile").
		Where("is_delete = ?", 0).
		Limit(0, 10).
		Order("uid asc").
		Value()
	fmt.Printf(" Use Value() function : %+v", resVal)

	// Array 返回数组
	resArray, err := g.Model("tb_user").
		Fields("uid, username, mobile").
		Where("is_delete = ?", 0).
		Limit(0, 10).
		Order("uid asc").
		Array()
	fmt.Printf(" Use Array() function : %+v", resArray)

	var resp struct {
		All   interface{}
		Value interface{}
		Array interface{}
	}

	resp.All = resAll
	resp.Array = resArray
	resp.Value = resVal

	return resp, err
}

// QueryOr or 查询
func (s *sDB) QueryOr(uid int64, mobile string) (res interface{}, err error) {
	result, err := g.Model("tb_user").
		Fields("uid, username, mobile").
		Where("uid = ?", uid).WhereOr("mobile = ?", mobile).
		Limit(0, 5).OrderDesc("uid").All()

	return result, err
}

// QueryCount 统计记录数
func (s *sDB) QueryCount() (int, error) {
	result, err := g.Model("tb_user").Where("is_delete = ?", 0).
		Count()

	return result, err
}

// OrmScan 按照一个 struct 或者 struct 数组返回
func (s *sDB) OrmScan(uid int64) (model.UserInfoOut, error) {
	var (
		data  model.UserInfoOut
		array []*model.UserInfoOut
	)

	// 按照 struct 返回
	err := g.Model("tb_user").
		Fields("uid, username, mobile, insert_at as register_time").
		Where("uid = ?", uid).Scan(&data)

	err = g.Model("tb_user").
		Fields("uid, username, mobile, insert_at as register_time").
		Scan(&array)

	fmt.Printf("struct array : %+v", array)

	return data, err
}

// QueryInnerJoin 内联查询
func (s *sDB) QueryInnerJoin(uid int64) (interface{}, error) {
	result, err := g.Model("tb_user tu").
		InnerJoin("tb_order to", "tu.uid = to.uid").
		Fields("tu.uid, tu.username, tu.mobile, to.oid, to.detail").
		Where("tu.uid = ?", uid).All()

	return result, err
}

// QueryLeftJoin 左联查询
func (s *sDB) QueryLeftJoin(uid int64) (interface{}, error) {
	result, err := g.Model("tb_user tu").
		LeftJoin("tb_order to", "tu.uid = to.uid").
		Fields("tu.uid, tu.username, tu.mobile, to.oid, to.detail").
		Where("tu.uid = ?", uid).All()

	return result, err
}

// QueryRightJoin 右联查询
func (s *sDB) QueryRightJoin(uid int64) (interface{}, error) {
	result, err := g.Model("tb_user tu").
		RightJoin("tb_order to", "tu.uid = to.uid").
		Fields("tu.uid, tu.username, tu.mobile, to.oid, to.detail").
		Where("tu.uid = ?", uid).All()

	return result, err
}

// QueryOrderAsc order 升序排列
func (s *sDB) QueryOrderAsc() (interface{}, error) {
	// 升序排列
	result, err := g.Model("tb_user").OrderAsc("uid").All()

	return result, err
}

// QueryOrderDesc order 降序排列
func (s *sDB) QueryOrderDesc() (interface{}, error) {
	// 升序排列
	result, err := g.Model("tb_user").OrderDesc("uid").All()

	return result, err
}

// QueryGroup 分组
func (s *sDB) QueryGroup() (interface{}, error) {
	// 降序排列
	result, err := g.Model("tb_order").Group("uid").All()

	return result, err
}

// QueryIn in查询
func (s *sDB) QueryIn(uids []int) (interface{}, error) {
	// in
	result, err := g.Model("tb_user").WhereIn("uid", g.Slice{uids}).All()
	// 等价于 SELECT * FROM tb_user WHERE uid in (1, 2, 3)
	/*
	* WhereNotIn() == uid Not IN (1,2,3)
	* WhereOrIn == OR uid IN (1, 2, 3)
	* WHereOrNotIn == OR uid NOT IN (1, 2, 3)
	 */

	return result, err
}

// QueryLike like查询
func (s *sDB) QueryLike(keyword string) (interface{}, error) {
	result, err := g.Model("tb_user").WhereLike("username", keyword+"%").All()
	/*
	* WhereNotLike() == username Not LIKE 'john%'
	* WhereOrLike= OR username LIKE 'john%'
	* WHereOrNotLike == OR username Not LIKE 'john%'
	 */

	return result, err
}

// QueryAggregate 使用聚合函数
func (s *sDB) QueryAggregate() (interface{}, error) {
	// max
	resMax, err := g.Model("tb_user").
		Where("is_delete = ?", 0).
		Max("uid")
	if err != nil {
		return nil, err
	}

	// min
	resMin, err := g.Model("tb_user").
		Where("is_delete = ?", 0).
		Min("uid")
	if err != nil {
		return nil, err
	}

	// avg
	resAvg, err := g.Model("tb_user").
		Where("is_delete = ?", 0).
		Avg("uid")
	if err != nil {
		return nil, err
	}

	// sum
	resSum, err := g.Model("tb_user").
		Where("is_delete = ?", 0).
		Sum("uid")

	var resp struct {
		Max float64
		Min float64
		Avg float64
		Sum float64
	}

	resp.Max = resMax
	resp.Min = resMin
	resp.Avg = resAvg
	resp.Sum = resSum

	return resp, err
}

// QueryBetween between 查询
func (s *sDB) QueryBetween(minVal, maxVal int64) (interface{}, error) {
	result, err := g.Model("tb_user").
		WhereBetween("uid", minVal, maxVal).
		All()

	/*
	* WhereNotBetween() == uid Not BETWEEN 1 AND 20
	* WhereOrBetween= OR uid BETWEEN 1 AND 20
	* WhereOrNotBetween == OR uid Not BETWEEN 1 AND 20
	 */

	return result, err
}

// QueryNull null 查询
func (s *sDB) QueryNull() (interface{}, error) {
	result, err := g.Model("tb_user").
		WhereNotNull("uid").
		All()

	/*
	* WhereNotNull() == uid IS NOT NULL
	* WhereOrNull= OR uid IS NULL
	* WhereOrNotNull == OR uid IS NOT NULL
	 */

	return result, err
}
