package db

import (
	"context"
	"demo/internal/model"
	"demo/internal/model/do"
	"demo/internal/service"
	"github.com/gogf/gf/v2/frame/g"
)

type (
	sDB struct{}
)

func New() *sDB {
	ctx := context.TODO()
	g.Log().Line().Print(ctx, "this is the short file name with its line number")
	return &sDB{}
}

func init() {
	service.RegisterDB(New())
}

func (s *sDB) Insert(in model.InsertNewRecordIn) (id int64, err error) {
	/**
	 * 普通的 insert
	 */

	result, err := g.Model("tb_user").Insert(do.TbUser{
		Username: in.Username,
		Password: in.Password,
		Mobile:   in.Mobile,
	})
	if err != nil {
		return 0, err
	}

	uid, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return uid, nil

	//InsertAndGetId 插入并返回自增字段的id
	//uid, err = g.Model("tb_user").InsertAndGetId(do.TbUser{
	//	Username: in.Username,
	//	Password: in.Password,
	//	Mobile:   in.Mobile,
	//})

	return uid, err
}

// Replace function
/**
 * Replace 使用 orm 里面的 replace into 方法，如果传了主键，那么就会删除原有的记录，然后写入一条行的记录
 */
func (s *sDB) Replace(in model.ReplaceRecordIn) error {
	// 不使用 data 方法
	_, err := g.Model("tb_user").Replace(do.TbUser{
		Uid:      in.Uid,
		Username: in.Username,
		Password: in.Password,
		Mobile:   in.Mobile,
	})

	// 使用 data 方法
	data := do.TbUser{
		Uid:      in.Uid,
		Username: in.Username,
		Password: in.Password,
		Mobile:   in.Mobile,
	}
	//_, err = g.Model("tb_user").OmitEmptyData().Data(data).Replace()
	_, err = g.Model("tb_user").OmitEmpty().Data(data).Replace()
	/*
	 * func (m *Model) OmitEmpty() *Model
	 * func (m *Model) OmitEmptyWhere() *Model
	 * func (m *Model) OmitEmptyData() *Model
	 * OmitEmpty方法会同时过滤 Where 及 Data 中的空值数据
	 * 而通过 OmitEmptyWhere / OmitEmptyData 方法可以执行特定的字段过滤
	 */

	return err
}

// SingleSave 使用INSERT INTO语句进行数据库写入，如果写入的数据中存在主键或者唯一索引时，更新原有数据，否则写入一条新数据
func (s *sDB) SingleSave(in model.SaveRecordIn) error {
	// 不使用 data 方法
	_, err := g.Model("tb_user").Save(do.TbUser{
		Uid:      in.Uid,
		Username: in.Username,
		Password: in.Password,
		Mobile:   in.Mobile,
	})

	return err
}

// OrmSave 使用INSERT INTO语句进行数据库写入，如果写入的数据中存在主键或者唯一索引时，更新原有数据，否则写入一条新数据
func (s *sDB) OrmSave(in model.SaveRecordIn) error {
	// 不使用 data 方法
	//_, err := g.Model("tb_user").Save(do.TbUser{
	//	Uid:      in.Uid,
	//	Username: in.Username,
	//	Password: in.Password,
	//	Mobile:   in.Mobile,
	//})

	// 使用 data 方法
	_, err := g.Model("tb_user").OmitEmptyData().Data(do.TbUser{
		Uid:      in.Uid,
		Username: in.Username,
		Password: in.Password,
		Mobile:   in.Mobile,
	}).Save()

	return err
}

// BatchSave 批量插入
func (s *sDB) BatchSave(in []*model.InsertNewRecordIn) error {
	// 这里的如果想直接使用 in 的话，struct 的定义要和 do 里面的 TbUser 结构对应才能直接使用
	_, err := g.Model("tb_user").Save(in)

	return err
}

// Update 更新操作
func (s *sDB) Update(in model.UpdateIn) error {
	// g.Map password 和 uid 字段要对应数据表里面的字段，大小写要一致
	_, err := g.Model("tb_user").Data(g.Map{"password": in.Password}).Where("uid = ?", in.Uid).Update()
	// 这个链式操作 等价于 UPDATE tb_user SET password = 'passowrd' WHERE uid = 'uid'

	return err
}

// Delete 删除操作 Delete()
func (s *sDB) Delete(uid int64) error {
	_, err := g.Model("tb_user").Where("uid", uid).Delete()
	//_, err = g.Model("tb_user").Where("uid < ", uid).Delete()

	return err
}
