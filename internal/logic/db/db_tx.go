package db

import (
	"context"
	"demo/internal/model"
	"demo/internal/model/do"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

/**
* 封装事务的操作
 */

// TransactionClosure 闭包式提交事务，推荐此写法
func (s *sDB) TransactionClosure(ctx context.Context, in model.TxIn) (int64, error) {
	var (
		userID int64
	)

	/**
	 * 当给定的闭包方法返回的error为nil时，那么闭包执行结束后当前事务自动执行Commit提交操作；否则自动执行Rollback回滚操作
	 */
	err := g.DB().Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		result, err := tx.Model("tb_user").Insert(do.TbUser{
			Username: in.Username,
			Password: in.Password,
			Mobile:   in.Mobile,
		})

		if err != nil {
			return err
		}

		userID, err = result.LastInsertId()
		if err != nil {
			return err
		}

		// 更新 order 表
		_, err = tx.Model("tb_order").
			Data(g.Map{"detail": in.Detail}).
			Where("oid = ?", in.Oid).
			Update()

		return err
	})

	return userID, err
}

// TransactionChain 事务的链式写法，这个写法需要靠 defer 来处理回滚和提交事务,不太推荐该写法
func (s *sDB) TransactionChain(ctx context.Context, in model.TxIn) (int64, error) {
	var (
		uid int64
	)

	tx, err := g.DB().Begin(ctx)
	if err != nil {
		return uid, err
	}

	// 方法退出时检验返回值，
	// 如果结果成功则执行tx.Commit()提交,
	// 否则执行tx.Rollback()回滚操作。
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	data := do.TbUser{
		Username: in.Username,
		Password: in.Password,
		Mobile:   in.Mobile,
	}

	result, err := tx.Insert("tb_user", data)
	if err != nil {
		return uid, err
	}

	// get insert id
	uid, err = result.LastInsertId()
	if err != nil {
		return 0, err
	}

	_, err = tx.Model("tb_order").
		Data(g.Map{"detail": in.Detail}).
		Where("oid = ?", in.Oid).
		Update()

	return uid, err
}
