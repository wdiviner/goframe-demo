package controller

import (
	"context"
	v1 "demo/api/v1"
	"demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// QuerySingleRecord 方法
func (c *cDemoDB) QuerySingleRecord(ctx context.Context, req *v1.QuerySingleRecordReq) (res *v1.QuerySingleRecordRes, err error) {
	uid := req.Uid
	mobile := req.Mobile

	result, err := service.DB().QuerySingleRecord(uid, mobile)
	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "query single record failed")
	}

	resp := &v1.QuerySingleRecordRes{
		Result: result,
	}

	return resp, gerror.NewCode(gcode.CodeOK, "query single record success!")
}

// QueryMultiRecord 方法
func (c *cDemoDB) QueryMultiRecord(ctx context.Context, req *v1.QueryMultiRecordReq) (res *v1.QueryMultiRecordRes, err error) {

	result, err := service.DB().QueryMultiRecord()

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "query multi record failed")
	}

	resp := &v1.QueryMultiRecordRes{
		List: result,
	}

	return resp, gerror.NewCode(gcode.CodeOK, "query multi record success!")
}

// QueryOr 方法
func (c *cDemoDB) QueryOr(ctx context.Context, req *v1.QueryOrReq) (res *v1.QueryOrRes, err error) {
	uid := req.Uid
	mobile := req.Mobile

	result, err := service.DB().QueryOr(uid, mobile)

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "query or record failed")
	}

	resp := &v1.QueryOrRes{
		Result: result,
	}

	return resp, gerror.NewCode(gcode.CodeOK, "query or record success!")
}

// QueryCount 方法
func (c *cDemoDB) QueryCount(ctx context.Context, req *v1.QueryCountReq) (res *v1.QueryCountRes, err error) {
	result, err := service.DB().QueryCount()

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "query or record failed")
	}

	resp := &v1.QueryCountRes{
		Result: result,
	}

	return resp, gerror.NewCode(gcode.CodeOK, "query or record success!")
}

// QueryScan 方法
func (c *cDemoDB) QuerySacn(ctx context.Context, req *v1.QueryScanReq) (res *v1.QueryScanRes, err error) {
	uid := req.Uid
	result, err := service.DB().OrmScan(uid)

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "query or record failed")
	}

	resp := &v1.QueryScanRes{
		Result: result,
	}

	return resp, gerror.NewCode(gcode.CodeOK, "query or record success!")
}
