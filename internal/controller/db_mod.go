package controller

import (
	"context"
	v1 "demo/api/v1"
	"demo/internal/model"
	"demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

var (
	// DemoDB 实例化一个 controller
	DemoDB = cDemoDB{}
)

// 定义一个 controller
type cDemoDB struct{}

// Insert insert 的方法
func (c *cDemoDB) Insert(ctx context.Context, req *v1.InsertReq) (res *v1.InsertRes, err error) {
	in := model.InsertNewRecordIn{
		Username: req.Username,
		Password: req.Password,
		Mobile:   req.Mobile,
	}

	uid, err := service.DB().Insert(in)

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "insert failed")
	}

	res = &v1.InsertRes{
		Uid: uid,
	}

	return res, gerror.NewCode(gcode.CodeOK, "insert success!")
}

// Replace replace 的方法
func (c *cDemoDB) Replace(ctx context.Context, req *v1.ReplaceReq) (res *v1.ReplaceRes, err error) {
	in := model.ReplaceRecordIn{
		Uid:      req.Uid,
		Username: req.Username,
		Password: req.Password,
		Mobile:   req.Mobile,
	}

	err = service.DB().Replace(in)

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "replace failed")
	}

	return nil, gerror.NewCode(gcode.CodeOK, "replace success!")
}

// Save 的方法
func (c *cDemoDB) Save(ctx context.Context, req *v1.SaveReq) (res *v1.SaveRes, err error) {
	in := model.SaveRecordIn{
		Uid:      req.Uid,
		Username: req.Username,
		Password: req.Password,
		Mobile:   req.Mobile,
	}

	err = service.DB().OrmSave(in)

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "save failed")
	}

	return nil, gerror.NewCode(gcode.CodeOK, "save success!")
}

// Update update 方法
func (c *cDemoDB) Update(ctx context.Context, req *v1.UpdateReq) (res *v1.UpdateRes, err error) {
	in := model.UpdateIn{
		Uid:      req.Uid,
		Password: req.Password,
	}

	err = service.DB().Update(in)

	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "update failed")
	}

	return nil, gerror.NewCode(gcode.CodeOK, "update success!")
}

// Delete 方法
func (c *cDemoDB) Delete(ctx context.Context, req *v1.DeleteReq) (res *v1.DeleteRes, err error) {
	uid := req.Uid

	err = service.DB().Delete(uid)
	if err != nil {
		g.Log().Errorf(ctx, "logic err : %v", err)
		return nil, gerror.NewCode(gcode.CodeOperationFailed, "delete failed")
	}

	return nil, gerror.NewCode(gcode.CodeOK, "delete success!")
}
