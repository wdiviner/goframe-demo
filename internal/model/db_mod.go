package model

type InsertNewRecordIn struct {
	Username string
	Password string
	Mobile   string
}

type ReplaceRecordIn struct {
	Uid      int64
	Username string
	Password string
	Mobile   string
	IsDelete int8
}

type SaveRecordIn struct {
	Uid      int64
	Username string
	Password string
	Mobile   string
	IsDelete int8
}

type UpdateIn struct {
	Uid      int64
	Password string
}
