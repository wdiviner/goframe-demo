package model

type TxIn struct {
	Username string
	Password string
	Mobile   string
	Oid      int64
	Detail   int64
}
