package model

import "github.com/gogf/gf/v2/os/gtime"

type UserInfoOut struct {
	Uid          int64
	Username     string
	Mobile       string
	RegisterTime gtime.Time
}
