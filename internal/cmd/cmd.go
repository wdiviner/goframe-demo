package cmd

import (
	"context"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gcmd"

	"demo/internal/controller"
)

// Middleware 解决中文乱码的问题
func Middleware(r *ghttp.Request) {
	r.Middleware.Next()
	// 中间件处理逻辑
	r.Response.Header().Set("Content-Type", "application/json;charset=utf-8")
}

var (
	Main = gcmd.Command{
		Name:  "main",
		Usage: "main",
		Brief: "start http server",
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			s := g.Server()
			s.Group("/", func(group *ghttp.RouterGroup) {
				group.Middleware(ghttp.MiddlewareHandlerResponse)
				group.Bind(
					controller.Hello,
					controller.DemoDB,
				)
			})
			s.Use(Middleware)
			s.Run()
			return nil
		},
	}
)
