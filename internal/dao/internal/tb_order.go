// ==========================================================================
// Code generated by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// TbOrderDao is the data access object for table tb_order.
type TbOrderDao struct {
	table   string         // table is the underlying table name of the DAO.
	group   string         // group is the database configuration group name of current DAO.
	columns TbOrderColumns // columns contains all the column names of Table for convenient usage.
}

// TbOrderColumns defines and stores column names for table tb_order.
type TbOrderColumns struct {
	Oid      string // 订单id
	Detail   string // 订单详情
	Uid      string // 订单关联用户id
	IsDelete string // 0 - 记录正常 1 - 记录被删除
	InsertAt string //
	UpdateAt string //
	DeleteAt string //
}

// tbOrderColumns holds the columns for table tb_order.
var tbOrderColumns = TbOrderColumns{
	Oid:      "oid",
	Detail:   "detail",
	Uid:      "uid",
	IsDelete: "is_delete",
	InsertAt: "insert_at",
	UpdateAt: "update_at",
	DeleteAt: "delete_at",
}

// NewTbOrderDao creates and returns a new DAO object for table data access.
func NewTbOrderDao() *TbOrderDao {
	return &TbOrderDao{
		group:   "default",
		table:   "tb_order",
		columns: tbOrderColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *TbOrderDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *TbOrderDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *TbOrderDao) Columns() TbOrderColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *TbOrderDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *TbOrderDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *TbOrderDao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
