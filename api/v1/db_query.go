package v1

import "github.com/gogf/gf/v2/frame/g"

type QuerySingleRecordReq struct {
	g.Meta `path:"/db/query/single" method:"get" tags:"query single" summary:"use orm query"`
	Uid    int64  `v:"required"`
	Mobile string `v:"phone"`
}

type QuerySingleRecordRes struct {
	Result interface{} `json:"result" dc:"响应结果"`
}

type QueryMultiRecordReq struct {
	g.Meta `path:"/db/query/multi" method:"get" tags:"query multi" summary:"use orm query"`
}

type QueryMultiRecordRes struct {
	List interface{} `json:"list" dc:"查询结果集"`
}

type QueryOrReq struct {
	g.Meta `path:"/db/query/or" method:"get" tags:"query or" summary:"use orm or"`
	Uid    int64  `v:"required"`
	Mobile string `v:"phone"`
}

type QueryOrRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryCountReq struct {
	g.Meta `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryCountRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryScanReq struct {
	g.Meta `path:"/db/query/scan" method:"get" tags:"query count" summary:"use orm count"`
	Uid    int64
}

type QueryScanRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryInnerJoinReq struct {
	g.Meta `path:"/db/query/innerjoin" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryInnerJoinRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryLeftJoinReq struct {
	g.Meta `path:"/db/query/leftjoin" method:"get" tags:"query count" summary:"use orm count"`
	Uid    int64
}

type QueryLeftJoinRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryRightJoinReq struct {
	g.Meta `path:"/db/query/rightjoin" method:"get" tags:"query count" summary:"use orm count"`
	Uid    int64
}

type QueryRightJoinRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryOrderAscReq struct {
	g.Meta `path:"/db/query/orderasc" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryOrderAscRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryOrderDescReq struct {
	g.Meta `path:"/db/query/orderdesc" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryOrderDescRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryGroupDescReq struct {
	g.Meta `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryGroupDescRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryInReq struct {
	g.Meta `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
	Uids   []int
}

type QueryInRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryLikeReq struct {
	g.Meta  `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
	Keyword string
}

type QueryLikeRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryAggregateReq struct {
	g.Meta `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryAggregateRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryBetweenReq struct {
	g.Meta `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
	MinVal int64
	MaxVal int64
}

type QueryBetweenRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}

type QueryNullReq struct {
	g.Meta `path:"/db/query/count" method:"get" tags:"query count" summary:"use orm count"`
}

type QueryNullRes struct {
	Result interface{} `json:"result" dc:"查询结果集"`
}
