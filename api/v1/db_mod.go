package v1

import "github.com/gogf/gf/v2/frame/g"

type InsertReq struct {
	g.Meta   `path:"/db/insert" method:"post" tags:"insert" summary:"use orm insert"`
	Username string `v:"required|length:1,30"`
	Password string `v:"required"`
	Mobile   string `v:"phone"`
}

type InsertRes struct {
	Uid int64 `json:"uid" dc:"用户id"`
}

type ReplaceReq struct {
	g.Meta   `path:"/db/replace" method:"post" tags:"replace" summary:"use orm replace"`
	Uid      int64
	Username string `v:"required|length:1,30"`
	Password string `v:"required"`
	Mobile   string `v:"phone"`
}

type ReplaceRes struct{}

type SaveReq struct {
	g.Meta   `path:"/db/save" method:"post" tags:"save" summary:"use orm replace"`
	Uid      int64
	Username string `v:"required|length:1,30"`
	Password string `v:"required"`
	Mobile   string `v:"phone"`
}

type SaveRes struct{}

type UpdateReq struct {
	g.Meta   `path:"/db/update" method:"post" tags:"update" summary:"use orm update"`
	Uid      int64  `v:"required"`
	Password string `v:"required"`
}

type UpdateRes struct{}

type DeleteReq struct {
	g.Meta `path:"/db/delete" method:"post" tags:"save" summary:"use orm replace"`
	Uid    int64
}

type DeleteRes struct{}
